# Terraform 3 Teir




## Description
 This terraform consist of 2 parts, the first one is configuration and another one is module, we use this architecture to make sure that every environment are spawned using the same way between another.

## How to run
`cd configuration/sandbox`
`terraform init`
`terraform apply`

## Configuration
The configuration is the current running configuration, inside this folder it will separated by the Environment, for this project it will only have sandbox environment


*  configuration
      * env_name
        * application-stack.tf
        * network-stack.tf
*  module
      * network_type (3-tier-stack)
      * resource
	      * resource_type (fullstack)

### application-stack.tf

#### Required Variable
| Variable| Description| Default|
| --------|---------|-------|
| region| Region where the resources will be spawned.| -    |
| **service**| Map of service configuration (see below).| -    |
| **alb**| Map of alb configuration (see below).| -    |
#### Required Variable/service
The format for service is like below example
```
<application_name> = {
	alb_host = <host_for_alb>
	alb_context_path = <url_path_for_alb>
	service_port = <service_port>
	health_check = <health_check_path>
	instance = {
		subnets = [<subnets_for_ec2s_to_spawn>]
		instance_type = <instance_type>
		min_instance = <no_of_minimum_instance>
		desired_capacity = <no_of_desired_instances>
		max_instance = <no_of_maximum_instance>
		ami = <ami_for_ec2_instance>
		user_data = <base_64_encoded_user_data_to_start_ec2>
	}
}
```
#### Required Variable/alb
The format for service is like below example
```
alb = {
	name = <name_of_your_alb>
	env= <the_env_name_of_alb>
	public_subnets= <public_subnet_where_alb_will_be_spawned>
}
```
#### Add new ec2 application
In the application stack, you can add more application by adding the application configuration inside service map, in below example I have added the web tier in public subnet, add the user data to mkdir /tmp/app or you can docker run your image or anything to start your application.
````

data  "template_file"  "application-tier"  {
	template = <<EOF
	mkdir  /tmp/app
	EOF
}

module  "fullstack"  {
	source = "../../module/resource/fullstack"
	
	region = "ap-southeast-1"
	application-tier =  {
		alb_host            = "api.danusarf.com"
		alb_context_path    = "/v1/*"
		service_port        = "8080"
		health_check        = "/health"
		instance            = { 
		    subnets             = [module.network.subnet_private_a_id,module.network.subnet_private_b_id,module.network.subnet_private_c_id]
		    instance_type       = "t2.nano"
		    min_instance        = "1"
		    desired_capacity    = "1"
		    max_instance        = "1"
		    ami                 = "ami-05c64f7b4062b0a21"
		    user_data           = "${base64encode(data.template_file.application-tier.rendered)}"
		}
	}
	alb = {
		name = "astro-poc-alb"
		env = "poc"
		public_subnets = [module.network.subnet_public_a_id,module.network.subnet_public_b_id,module.network.subnet_public_c_id]
	}
}
...

````

### network-stack.tf
#### Required Variable

| Variable| Description| Default|
| --------|---------|-------|
| enable_dns_support| A  boolean  flag  to  enable/disable  DNS  support  in  the  VPC.| true    |
| enable_dns_hostnames| A  boolean  flag  to  enable/disable  DNS  hostnames  in  the  VPC| false    |
| enable_classiclink| A  boolean  flag  to  enable/disable  ClassicLink  for  the  VPC.  Only  valid  in  regions  and  accounts  that  support  EC2  Classic| false    |
| name| Name  of  the  VPC  will  be  created| -    |
| cidr_blocks| cidr blocks of the vpc| 10.0.0.0/16    |
| cidr_blocks_public| List of 3 cidr block of the public subnet with following format: [az-a, az-b, az-c]| ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]    |
| cidr_blocks_private| List of 3 cidr block of the private subnet with following format: [az-a, az-b, az-c]| ["10.0.3.0/24", "10.0.4.0/24", "10.0.5.0/24"]    |
| cidr_blocks_db| List of 3 cidr block of the db subnet with following format: [az-a, az-b, az-c]| ["10.0.6.0/24", "10.0.7.0/24", "10.0.8.0/24"]    |
| tags| map of tags, for tagging the resources| -    |
| region| region where the resources will be spawned| ap-southeast-1    |
