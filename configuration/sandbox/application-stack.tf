data "template_file" "application-tier" {
  template = <<EOF
    mkdir /tmp/app
  EOF
}

data "template_file" "web-tier" {
  template = <<EOF
    mkdir /tmp/web
  EOF
}


module "fullstack" {
    source  = "../../module/resource/fullstack"

    alb     = {
        name            = "astro-poc-alb"
        env             = "poc"
        public_subnets  = [module.network.subnet_public_a_id,module.network.subnet_public_b_id,module.network.subnet_public_c_id]
    }

    region  = "ap-southeast-1"
    service = {

        application-tier =  {
            alb_host            = "api.danusarf.com"
            alb_context_path    = "/v1/*"
            service_port        = "8080"
            health_check        = "/health"
            instance            = { 
                subnets             = [module.network.subnet_private_a_id,module.network.subnet_private_b_id,module.network.subnet_private_c_id]
                instance_type       = "t2.nano"
                min_instance        = "1"
                desired_capacity    = "1"
                max_instance        = "1"
                ami                 = "ami-05c64f7b4062b0a21"
                user_data           = "${base64encode(data.template_file.application-tier.rendered)}"
            }
        }

        web-tier = {
            alb_host            = "web.danusarf.com"
            alb_context_path    = "/*"
            service_port        = "8080"
            health_check        = "/"
            instance            = { 
                subnets             = [module.network.subnet_public_a_id,module.network.subnet_public_b_id,module.network.subnet_public_c_id]
                instance_type       = "t2.nano"
                min_instance        = "1"
                desired_capacity    = "1"
                max_instance        = "1"
                ami                 = "ami-05c64f7b4062b0a21"
                user_data           = "${base64encode(data.template_file.web-tier.rendered)}"
            }
        }

    }   
}