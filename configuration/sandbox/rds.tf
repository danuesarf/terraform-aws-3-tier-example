data "aws_subnet" "selected_db_subnet" {
  id = module.network.subnet_db_a_id
}

resource "aws_security_group" "allow_app_to_rds" {
  name        = "db-sg"
  description = "Allow ALB to Access Autoscaling Application"
  vpc_id      = data.aws_subnet.selected_db_subnet.vpc_id

  ingress {
    # TLS (change to whatever ports you need)
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [module.fullstack.application_security_group_id["application-tier-application-sg"]]
  }
}

module "db" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier = "demodb"

  engine            = "mysql"
  engine_version    = "5.7.19"
  instance_class    = "db.t2.micro"
  allocated_storage = 5

  name     = "demodb"
  username = "user"
  password = "YourPwdShouldBeLongAndSecure"
  port     = "3306"

  iam_database_authentication_enabled = true

  vpc_security_group_ids = [aws_security_group.allow_app_to_rds.id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  tags = {
    Owner       = "user"
    Environment = "dev"
  }

  # DB subnet group
  subnet_ids = [module.network.subnet_db_a_id, module.network.subnet_db_b_id]

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  skip_final_snapshot = true

  # Database Deletion Protection
  deletion_protection = false

  parameters = [
    {
      name = "character_set_client"
      value = "utf8"
    },
    {
      name = "character_set_server"
      value = "utf8"
    }
  ]

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]
}