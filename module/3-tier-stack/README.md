This module is to create AWS 3 Tier Network.


Example to call this module use this following format

module "network" {
    source = "../../module/3-tier-stack"
    
    name = "poc-astro-test"
    tags = {
        Name                = "poc-astro-test"
        Environmet          = "poc"
        BU                  = "Astro"
        Project             = "test"

    }
}