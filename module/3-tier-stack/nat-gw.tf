resource "aws_eip" "zone_a" {
  tags    = var.tags
}
resource "aws_nat_gateway" "zone_a" {
  allocation_id = "${aws_eip.zone_a.id}"
  subnet_id     = aws_subnet.public_a.id

  tags = var.tags
}

resource "aws_eip" "zone_b" {
  tags    = var.tags
}
resource "aws_nat_gateway" "zone_b" {
  allocation_id = "${aws_eip.zone_b.id}"
  subnet_id     = aws_subnet.public_b.id

  tags = var.tags
}

resource "aws_eip" "zone_c" {
  tags    = var.tags
}
resource "aws_nat_gateway" "zone_c" {
  allocation_id = "${aws_eip.zone_c.id}"
  subnet_id     = aws_subnet.public_c.id

  tags = var.tags
}

