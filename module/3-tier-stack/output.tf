output "vpc_id" {
  value = "${aws_vpc.this.id}"
}
output "vpc_owner_id" {
  value = "${aws_vpc.this.owner_id}"
}
output "internet_gateway_id" {
  value = "${aws_internet_gateway.this.id}"
}
output "subnet_private_a_id" {
  value = aws_subnet.private_a.id
}
output "subnet_private_b_id" {
  value = aws_subnet.private_b.id
}
output "subnet_private_c_id" {
  value = aws_subnet.private_b.id
}

output "subnet_public_a_id" {
  value = aws_subnet.public_a.id
}
output "subnet_public_b_id" {
  value = aws_subnet.public_b.id
}
output "subnet_public_c_id" {
  value = aws_subnet.public_c.id
}

output "subnet_db_a_id" {
  value = aws_subnet.db_a.id
}
output "subnet_db_b_id" {
  value = aws_subnet.db_b.id
}
output "subnet_db_c_id" {
  value = aws_subnet.db_c.id
}