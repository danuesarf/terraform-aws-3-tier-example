resource "aws_subnet" "public_a" {
  vpc_id              = aws_vpc.this.id
  cidr_block          = var.cidr_blocks_public[0]
  availability_zone   = "${var.region}a"

  tags = var.tags
}
resource "aws_subnet" "public_b" {
  vpc_id              = aws_vpc.this.id
  cidr_block          = var.cidr_blocks_public[1]
  availability_zone   = "${var.region}b"

  tags = var.tags
}
resource "aws_subnet" "public_c" {
  vpc_id              = aws_vpc.this.id
  cidr_block          = var.cidr_blocks_public[2]
  availability_zone   = "${var.region}c"

  tags = var.tags
}

resource "aws_subnet" "private_a" {
  vpc_id              = aws_vpc.this.id
  cidr_block          = var.cidr_blocks_private[0]
  availability_zone   = "${var.region}a"

  tags = var.tags
}
resource "aws_subnet" "private_b" {
  vpc_id              = aws_vpc.this.id
  cidr_block          = var.cidr_blocks_private[1]
  availability_zone   = "${var.region}b"

  tags = var.tags
}
resource "aws_subnet" "private_c" {
  vpc_id              = aws_vpc.this.id
  cidr_block          = var.cidr_blocks_private[2]
  availability_zone   = "${var.region}c"

  tags = var.tags
}

resource "aws_subnet" "db_a" {
  vpc_id              = aws_vpc.this.id
  cidr_block          = var.cidr_blocks_db[0]
  availability_zone   = "${var.region}a"

  tags = var.tags
}
resource "aws_subnet" "db_b" {
  vpc_id              = aws_vpc.this.id
  cidr_block          = var.cidr_blocks_db[1]
  availability_zone   = "${var.region}b"

  tags = var.tags
}
resource "aws_subnet" "db_c" {
  vpc_id              = aws_vpc.this.id
  cidr_block          = var.cidr_blocks_db[2]
  availability_zone   = "${var.region}c"

  tags = var.tags
}