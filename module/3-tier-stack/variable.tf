variable "enable_dns_support" {
  default = "true"
  description = "A boolean flag to enable/disable DNS support in the VPC."
}
variable "enable_dns_hostnames" {
  default = "false"
  description = "A boolean flag to enable/disable DNS hostnames in the VPC"
}
variable "enable_classiclink" {
  default = "false"
  description = "A boolean flag to enable/disable ClassicLink for the VPC. Only valid in regions and accounts that support EC2 Classic"
}
variable "name" {
  description = "Name of the VPC will be created"
}
variable "cidr_blocks" {
  default = "10.0.0.0/16"
}

variable "cidr_blocks_public" {
  default = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
}

variable "cidr_blocks_private" {
  default = ["10.0.3.0/24", "10.0.4.0/24", "10.0.5.0/24"]
}

variable "cidr_blocks_db" {
  default = ["10.0.6.0/24", "10.0.7.0/24", "10.0.8.0/24"]
}
variable "tags" {
  
}
variable "region" {
  default = "ap-southeast-1"
}
