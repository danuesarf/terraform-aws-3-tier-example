resource "aws_vpc" "this" {
  enable_dns_support = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_classiclink = var.enable_classiclink
  cidr_block          = var.cidr_blocks
  tags = var.tags
}
