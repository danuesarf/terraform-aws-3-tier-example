data "aws_subnet" "selected" {
  id = "${var.alb.public_subnets[0]}"
}

resource "aws_lb" "this" {
  name                          = "${var.alb.name}"
  internal                      = false
  load_balancer_type            = "application"
  security_groups               = [aws_security_group.allow_outside_access_alb.id]
  subnets                       = var.alb.public_subnets

  enable_deletion_protection    = false

  tags = {
    Name        = var.alb.name
    Environment = var.alb.env
  }
}

resource "aws_lb_listener" "this" {

    load_balancer_arn = "${aws_lb.this.arn}"
    port              = "80"
    protocol          = "HTTP"

    default_action {
        type = "fixed-response"
        fixed_response {
            content_type = "text/plain"
            message_body = "RULE NOT DEFINED"
            status_code  = "404"
        }
    }

}

resource "aws_lb_listener_rule" "static" {
    for_each = var.service

    listener_arn = "${aws_lb_listener.this.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.this[each.key].arn}"
    }
    condition {
        path_pattern {
            values = [each.value.alb_context_path]
        }
    }
    condition {
        host_header {
            values = [each.value.alb_host]
        }
    }
}


resource "aws_lb_target_group" "this" {
    for_each = var.service

    name     = "${each.key}-tg"
    port     = each.value.service_port
    protocol = "HTTP"
    vpc_id   = data.aws_subnet.selected.vpc_id
}


resource "aws_security_group" "allow_outside_access_alb" {
  name        = var.alb.name
  description = "Allow Public to Access ALB"
  vpc_id      = data.aws_subnet.selected.vpc_id

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}