resource "aws_launch_template" "this" {
  for_each = var.service

  name_prefix             = each.key
  image_id                = each.value.instance.ami
  instance_type           = each.value.instance.instance_type
  user_data               = each.value.instance.user_data
  vpc_security_group_ids  = [aws_security_group.allow_alb_access_autoscale[each.key].id]
}
resource "aws_autoscaling_group" "this" {
  for_each = var.service

  availability_zones  = ["${var.region}a","${var.region}b","${var.region}c"]
  desired_capacity    = each.value.instance.desired_capacity
  max_size            = each.value.instance.max_instance
  min_size            = each.value.instance.min_instance
  vpc_zone_identifier = each.value.instance.subnets
  
  target_group_arns   = [aws_lb_target_group.this[each.key].arn]

  launch_template {
    id      = "${aws_launch_template.this[each.key].id}"
    version = "$Latest"
  }
}

resource "aws_security_group" "allow_alb_access_autoscale" {
  for_each = var.service

  name        = "${each.key}-application-sg"
  description = "Allow ALB to Access Autoscaling Application"
  vpc_id      = data.aws_subnet.selected.vpc_id

  ingress {
    # TLS (change to whatever ports you need)
    from_port       = each.value.service_port
    to_port         = each.value.service_port
    protocol        = "tcp"
    security_groups = [aws_security_group.allow_outside_access_alb.id]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}