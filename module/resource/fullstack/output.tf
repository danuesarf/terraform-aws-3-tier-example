output "application_security_group_id" {
    value = {
        for security_group in aws_security_group.allow_alb_access_autoscale:
        security_group.name => security_group.id
    }
}
